package br.edu.up;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class Consumidor {

  public static void main(String[] args) throws NamingException, JMSException {
   
    Context ctx = new InitialContext();
    Queue fila = (Queue) ctx.lookup("jms/FILA_DE_TESTE");
    ConnectionFactory cf = (ConnectionFactory) ctx.lookup("jms/RemoteConnectionFactory");
    
    try(Connection con = cf.createConnection("up", "positivo")){
      Session session = con.createSession(false, Session.AUTO_ACKNOWLEDGE);
      MessageConsumer consumidor = session.createConsumer(fila);
      
      con.start();
      
//      String str;
//      do{
//          TextMessage txtMsg = (TextMessage) consumidor.receive();
//          str = txtMsg.getText();
//          System.out.println("Mensagem: " + str);
//      } while (!str.equals("")); 
      
      con.close();
      
      consumidor.close();
      ctx.close();
    }
    
    
    
  }
}
