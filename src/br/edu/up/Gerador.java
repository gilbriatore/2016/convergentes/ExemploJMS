package br.edu.up;

import java.util.Scanner;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class Gerador {

  public static void main(String[] args) throws NamingException, JMSException {

    Context ctx = new InitialContext();
    Queue fila = (Queue) ctx.lookup("jms/FILA_DE_TESTE");
    ConnectionFactory cf = (ConnectionFactory) ctx.lookup("jms/RemoteConnectionFactory");

    try (Connection con = cf.createConnection("up", "positivo")) {

      Session session = con.createSession(false, Session.AUTO_ACKNOWLEDGE);
      MessageProducer gerador = session.createProducer(fila);

      Scanner scan = new Scanner(System.in);
      String str;
      do {
        System.out.println("Digite a mensagem:");
        str = scan.nextLine();
        if (!str.equals("sair")) {
          TextMessage txtMsg = session.createTextMessage(str);
          gerador.send(txtMsg);
        }
      } while (!str.equals("sair"));
      scan.close();
      gerador.close();
      ctx.close();
    }

  }
}
